class formData {
    constructor() {
        this.empData = new Object();
        this.modelOpen = true;
    }
    setFormData(empData) {
        this.empData = empData;
    }

    getFormData() {
        return this.empData;
    }
    setModelOpen(t){
        this.modelOpen = t;
    }
    getModelOpen(){
     return  this.modelOpen;
    }
}

export default new formData;
