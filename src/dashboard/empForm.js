import React, { Fragment, useState } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Modal from 'react-modal';
import formData from '../service';

const validation = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(70, 'Too Long!')
    .required('Required'),
  gender: Yup.string()
    .required('Required')
    .oneOf(['Male', 'Female'], 'Choose Vaild Gender'),
  age: Yup.number()
    .required('Required'),
  department: Yup.string()
    .required('Required'),
  designation: Yup.string()
    .required('Required'),
  joiningDate: Yup.date()
    .required('Required'),
});


function EmpForm() {
  var arr = new Array();
  const element = <Formik initialValues={{ name: '', gender: '', age: '', department: '', designation: '', joiningDate: '' }}
    validationSchema={validation}
    onSubmit={values => {
      console.log(values);
      formData.setFormData(values);
      formData.setModelOpen(false);
      localStorage.setItem('table', JSON.stringify(values));
    }} >

    {({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (

      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row ">
          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">Name*</label>
            <input type="text" className="form-control" placeholder="Enter" name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} />
          </div>
          {errors.name && touched.name && <ErrorModel error={errors.name} name='Name' />}

          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">Gender*</label>
            <select className="form-control" name="gender" onChange={handleChange} onBlur={handleBlur} value={values.gender}>
              <option>Select</option>
              <option>Male</option>
              <option>Female</option>
            </select>
          </div>

          {errors.gender && touched.gender && <ErrorModel error={errors.gender} name='Gender' />}

          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">Age*</label>
            <input type="number" className="form-control" name="age" placeholder="Enter" onChange={handleChange} onBlur={handleBlur} value={values.age} />
          </div>

          {errors.age && touched.age &&<ErrorModel error={errors.age} name='Age' />}

          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">Department*</label>
            <input type="text" className="form-control" placeholder="Enter" name="department" onChange={handleChange} onBlur={handleBlur} value={values.department} />
          </div>

          {errors.department && touched.department && <ErrorModel error={errors.department} name='Department' />}

          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">designation*</label>
            <input type="text" className="form-control" placeholder="Enter" name="designation" onChange={handleChange} onBlur={handleBlur} value={values.designation} />
          </div>

          {errors.designation && touched.designation && <ErrorModel error={errors.designation} name='Designation' />}

          <div className="form-group col-md-6">
            <label htmlFor="" className="mb-1">Joining Date*</label>
            <input type="date" className="form-control" placeholder="Enter" name="joiningDate" onChange={handleChange} onBlur={handleBlur} value={values.joiningDate} />
          </div>
          {errors.joiningDate && touched.joiningDate && <ErrorModel error={errors.joiningDate} name='JoiningDate' />}
          <button type="submit" className="btn btn-success btn-sm" >Save</button>
          <button type="button" className="btn btn-outline-danger btn-sm" >Cancel</button>
        </div>
      </form>)}
  </Formik>

  return (
    <Fragment>
      {element}
    </Fragment>
  );
}


const customStyles = {
  content: {
    top: '15%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

Modal.setAppElement('#dev')


function ErrorModel(props) {

  const [modalIsOpen, setIsOpen] = useState(true);

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <Fragment>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles} >

        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header ">
              <h5 className="modal-title">Error In {props.name}</h5>
              <button type="button" className="close" onClick={closeModal}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {props.error}
            </div>
          </div>
        </div>

      </Modal>
    </Fragment>
  );
}


export default EmpForm;