import React, { Fragment, useState, useEffect } from 'react';
import Modal from 'react-modal';
import './dashboard.css';
import EmpForm from './empForm';
import formData from '../service';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

Modal.setAppElement('#yourAppElement')


function EmployeeModel() {

    const [modalIsOpen, setIsOpen] = useState(false);
    const [arr, setArr] = useState([]);

    function openModal() {
        setIsOpen(true);
        document.getElementById('root').style.filter = 'blur(5px)'
    }

    function closeModal() {
        document.getElementById('root').style.filter = 'blur(0px)'
        var dd = localStorage.getItem('table');
        setArr(arr => arr.concat(dd));
        localStorage.setItem('table2', arr);
        var dd1 = localStorage.getItem('table2');
        setIsOpen(false);
    }

    return (
        <Fragment>
            <button className="btn btn-primary mt-4" data-toggle="modal" onClick={openModal}>
                <i className="fa fa-plus"></i>&nbsp;Add Employee </button>
            <Modal
                animationType="fade"
                transparent={false}
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles} >

                <div className="modal-dialog" role="document" >
                    <div className="modal-content">
                        <div className="modal-header ">
                            <h5 className="modal-title">Add Employee</h5>
                            <button type="button" className="close" onClick={closeModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <EmpForm />
                        </div>
                        <div className="modal-footer">
                        </div>
                    </div>
                </div>

            </Modal>
        </Fragment>
    );
}

function Dashboard() {

    return (
        <Fragment>
            <nav className="navbar navbar-expand-md navbar-light bg-light">
                <a className="navbar-brand" href="https://www.logic-square.com" target="_blank">
                    <img src="https://res.cloudinary.com/www-logic-square-com/image/upload/v1551945805/ls-logo.png" className="ls-logo" alt="LS Logo" />
                </a>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Page Name</a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="question-dashboard">
                            <div className="card mt-4 mb-3 mb-md-4">
                                <div className="card-body p-3">
                                    <h5 className="text-secondary mb-2">Available: <span
                                        className="font-weight-bold ml-1 text-dark">08</span></h5>
                                    <h5 className="text-secondary">Total: <span className="font-weight-bold ml-1 text-dark">50</span>
                                    </h5>
                                    <EmployeeModel />
                                </div>
                            </div>


                            <div className="table-responsive ">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Available</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>John Doe</td>
                                            <td>Testing</td>
                                            <td>
                                                <div className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                                    <label className="custom-control-label" htmlFor="customCheck1"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" className="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#addEmployeeModal">
                                                    <i className="fa fa-edit"></i>&nbsp; Edit  </button>
                                                <button type="button" className="btn btn-outline-danger btn-sm">
                                                    <i className="fa fa-trash"></i>&nbsp; Delete</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Peter Doe</td>
                                            <td>Deployment</td>
                                            <td>
                                                <div className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" id="customCheck2" />
                                                    <label className="custom-control-label" htmlFor="customCheck2"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" className="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#addEmployeeModal">
                                                    <i className="fa fa-edit"></i>&nbsp; Edit     </button>
                                                <button type="button" className="btn btn-outline-danger btn-sm">
                                                    <i className="fa fa-trash"></i>&nbsp; Delete</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}



export default Dashboard;
